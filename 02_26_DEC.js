var BinaryTree = require('./BinaryTree.js');

const findRightSibling = (node)=>{
 
  let binTree = new BinaryTree();
  binTree.addNodeWithLevels();
  let levels = binTree.levelOrderTraversal().toString().split(' ');
  levels = levels.slice(0,levels.length-1);
  levels=levels.map((val)=>{
   val =  val.split(',');
  
    val = val.filter((element)=>Number(element)!==0);
    return val;
  }).filter((element)=>element.indexOf(String(node))!=-1)[0];
 
 if(levels[levels.indexOf(String(node))+1] && levels.indexOf(String(node))!=-1 ){
   return levels[levels.indexOf(String(node))+1]
 }
 else
 {
   return null;
 }

}
console.log(findRightSibling(10));
console.log(findRightSibling(7));
console.log(findRightSibling(2));