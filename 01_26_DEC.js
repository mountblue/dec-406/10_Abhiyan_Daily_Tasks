var BinaryTree = require('./BinaryTree.js');

var binTree = new BinaryTree();
binTree.addNodeWithLevels()


let elements = [8,2]


function findAnsestors(node = binTree.root) {
   
    if(node==null){
        return null;
    }
    if(elements.includes(node.value)){
        return node.value;
    }
    let left = findAnsestors(node.left);
    
    let right = findAnsestors(node.right);
    
    if(left && right){
        return node.value;
    }
    if(!left && !right){
        return null
    }
    return left==null?right:left;   
}
console.log(findAnsestors());

